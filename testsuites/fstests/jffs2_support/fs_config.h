/*
 * Copyright (c) 2013 embedded brains GmbH.  All rights reserved.
 *
 * The license and distribution terms for this file may be
 * found in the file LICENSE in this distribution or at
 * http://www.rtems.org/license/LICENSE.
 */

#ifndef __JFFS2_SUPPORT_h
#define __JFFS2_SUPPORT_h

#define FILESYSTEM "JFFS2"

#endif
