/*
 * Copyright (c) 2018 embedded brains GmbH.  All rights reserved.
 *
 * The license and distribution terms for this file may be
 * found in the file LICENSE in this distribution or at
 * http://www.rtems.org/license/LICENSE.
 */

ssize_t _Console_simple_Read(
  rtems_libio_t *iop,
  void          *buffer,
  size_t         count
);
