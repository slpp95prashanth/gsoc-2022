/* Include the necessary header files */
#include "dcan_frame.h"
#include "soc_AM335x.h"
#include "beaglebone.h"
#include "hw_types.h"
#include "dcan.h"

#include <stdio.h>
#include <bsp/irq.h>

/******************************************************************************
**                       INTERNAL MACRO DEFINITIONS
******************************************************************************/
#define DCAN_NO_INT_PENDING               (0x00000000u)
#define DCAN_IN_CLK                       (24000000u)
#define DCAN_ERROR_OCCURED                (0x8000u)
#define DCAN_BIT_RATE                     (1000000u)
#define CAN_TX_MSG_EXTD_ID                (0x1000u)
#define CAN_TX_MSG_STD_ID                 (0x02u)

/******************************************************************************
**                       INTERNAL FUNCTION PROTOTYPES                     
******************************************************************************/
static void DCANAintcConfigure(void);
static void ConfigureDCAN(void);
static void DCANParityIsr(void);
static void DCANIsr0(void);

/******************************************************************************
**                       GLOBAL VARIABLE DEFINITIONS                   
******************************************************************************/
/* CAN frame details */
static unsigned int rxflag = (CAN_DATA_FRAME | CAN_MSG_DIR_RX);
static volatile unsigned int isrRxFlag = 1;
static volatile unsigned int isrFlag = 1;
static unsigned int canData[2];
static unsigned int canData1[2];
static unsigned int canId = 0;
static unsigned int bytes = 0;
can_frame entry, entry1;

/******************************************************************************
**                      INTERNAL FUNCTION DEFINITIONS
******************************************************************************/
int dcan_init(void)
{
    unsigned int index = 0;

    /* Enable the DCAN1 module clock */
    DCANModuleClkConfig();

    /* Perform the pinmux for DCAN1 */
    DCANPinMuxSetUp(1);

    /* Initialize the DCAN message RAM */
    DCANMsgRAMInit(1);

    /* Register the DCAN interrupts */
    DCANAintcConfigure();

    /* Perform the DCAN configuration */
    ConfigureDCAN();

    index = CAN_NUM_OF_MSG_OBJS;

    while(index--)
    {
        /* Invalidate all message objects in the message RAM */
        CANInValidateMsgObject(SOC_DCAN_1_REGS, index, DCAN_IF2_REG);
    }
#if 1
    entry.flag = rxflag;
    entry.id = canId;

    /* 
    ** Configure a receive message object to accept CAN 
    ** frames with standard ID.
    */
    CANMsgObjectConfig(SOC_DCAN_1_REGS, &entry);

    entry.flag = (CAN_EXT_FRAME | CAN_MSG_DIR_RX | CAN_DATA_FRAME);
    entry.id = canId;

    /*
    ** Configure a receive message object to accept CAN
    ** frames with extended ID.
    */
    CANMsgObjectConfig(SOC_DCAN_1_REGS, &entry);
#endif
    /* Start the CAN transfer */
    DCANNormalModeSet(SOC_DCAN_1_REGS);

    DCANTestModesEnable(SOC_DCAN_1_REGS, DCAN_TEST_LBACK);

    /* Enable the error interrupts */
    DCANIntEnable(SOC_DCAN_1_REGS, DCAN_ERROR_INT);

    /* Enable the interrupt line 0 of DCAN module */
    DCANIntLineEnable(SOC_DCAN_1_REGS, DCAN_INT_LINE0);

    entry.flag = (CAN_DATA_FRAME | CAN_MSG_DIR_TX);
    entry.id = CAN_TX_MSG_STD_ID;

    canData[0] = (unsigned int)'a';
    canData[1] = 'b'; 

    entry.dlc = (unsigned int)1;
    entry.data = (unsigned int*)canData;

    CANMsgObjectConfig(SOC_DCAN_1_REGS, &entry);    

    entry1.flag = (CAN_EXT_FRAME | CAN_DATA_FRAME | CAN_MSG_DIR_TX);
    entry1.id = CAN_TX_MSG_EXTD_ID;

    canData1[0] = (unsigned int)'w';
    canData1[1] = 'u'; 

    entry1.dlc = (unsigned int)3;
    entry1.data = (unsigned int*)canData1;

    CANMsgObjectConfig(SOC_DCAN_1_REGS, &entry1);    
}

/*
** This function will configure DCAN with the required parameters.
*/
static void ConfigureDCAN(void)
{
    /* Reset the DCAN module */
    DCANReset(SOC_DCAN_1_REGS);

    /* Enter the Initialization mode of CAN controller */
    DCANInitModeSet(SOC_DCAN_1_REGS);

    /* Enable the write access to the DCAN configuration registers */
    DCANConfigRegWriteAccessControl(SOC_DCAN_1_REGS, DCAN_CONF_REG_WR_ACCESS_ENABLE);

    /* Configure the bit timing values for CAN communication */
    CANSetBitTiming(SOC_DCAN_1_REGS, DCAN_IN_CLK, DCAN_BIT_RATE);

    /* Disable the write access to the DCAN configuration registers */
    DCANConfigRegWriteAccessControl(SOC_DCAN_1_REGS, DCAN_CONF_REG_WR_ACCESS_DISABLE);
}


/*
** DCAN Isr for Interrupt line 0.
*/
static void DCANIsr0(void)
{
    unsigned char *dataPtr;
    unsigned int index = 0;
    unsigned int errVal;
    unsigned int msgNum;

    unsigned int candata[2];

    printf("*** isr handler ***\r\n");

    //while (1);

    while(DCANIntRegStatusGet(SOC_DCAN_1_REGS, DCAN_INT_LINE0_STAT))
    {
        if(DCANIntRegStatusGet(SOC_DCAN_1_REGS, DCAN_INT_LINE0_STAT) == 
                               DCAN_ERROR_OCCURED)
        {
            /* Check the status of DCAN Status and error register */
            errVal = DCANErrAndStatusRegInfoGet(SOC_DCAN_1_REGS);       
            
            if(errVal & DCAN_MOD_IN_BUS_OFF_STATE)
            {
                printf("**DCAN is in Bus-off state**\n");

                /* 
                ** This feature will automatically get the CAN bus to bus-on 
                ** state once the error counters are below the error warning 
                ** limit. 
                */
                DCANAutoBusOnControl(SOC_DCAN_1_REGS, DCAN_AUTO_BUS_ON_ENABLE);
            }

            if(errVal & DCAN_ERR_WARN_STATE_RCHD)
            {
                printf("Atleast one of the error counters have");
                printf(" reached the error warning limit\n");
            }
        }
    
        if((DCANIntRegStatusGet(SOC_DCAN_1_REGS, DCAN_INT_LINE0_STAT) != 
                                DCAN_NO_INT_PENDING) && 
           ((DCANIntRegStatusGet(SOC_DCAN_1_REGS, DCAN_INT_LINE0_STAT) != 
                                DCAN_ERROR_OCCURED)))
        {
            /* Get the number of the message object which caused the interrupt */
            msgNum = DCANIntRegStatusGet(SOC_DCAN_1_REGS, DCAN_INT_LINE0_STAT);
    
            /* Interrupt handling for transmit objects */
            if(msgNum < (CAN_NUM_OF_MSG_OBJS/2))
            {
                /* Clear the Interrupt pending status */
                CANClrIntPndStat(SOC_DCAN_1_REGS, msgNum, DCAN_IF1_REG);

		    printf("tx intr\n");
            }

            if((msgNum >= (CAN_NUM_OF_MSG_OBJS/2)) && (msgNum < CAN_NUM_OF_MSG_OBJS))
            {
		    printf("rx intr\n");

                /* Read a received message from message RAM to interface register */
                CANReadMsgObjData(SOC_DCAN_1_REGS, msgNum, (unsigned int*) canData, 
                                  DCAN_IF2_REG);

                if((DCANIFArbStatusGet(SOC_DCAN_1_REGS, DCAN_IF2_REG) & 
                    DCAN_EXT_ID_READ) == DCAN_29_BIT_ID)
                {
                    entry.flag = (CAN_EXT_FRAME | CAN_DATA_FRAME | CAN_MSG_DIR_TX); 
                    entry.id = CAN_TX_MSG_EXTD_ID;
                }
                else
                {
                    entry.flag = (CAN_DATA_FRAME | CAN_MSG_DIR_TX);
                    entry.id = CAN_TX_MSG_STD_ID;
                }

                /* Clear the Interrupt pending status */
                CANClrIntPndStat(SOC_DCAN_1_REGS, msgNum, DCAN_IF2_REG);
    
                dataPtr = (unsigned char*) canData;

                printf("Data received = ");

                bytes = (DCANIFMsgCtlStatusGet(SOC_DCAN_1_REGS, DCAN_IF2_REG) & 
                                               DCAN_DAT_LEN_CODE_READ);

		    
                /* Print the received data bytes on the UART console */
                for(index = 0; index < bytes; index++)
                {
                    printf("%c", *dataPtr++);
                }

		    printf("Num of bytes = %d\n", bytes);
 
                printf("\r\n");

                isrFlag = 0;

                /* Populate the can_frame structure with the CAN frame information */
                entry.dlc = bytes;
                entry.data = (unsigned int*)canData;

                /* Configure a transmit message object */
//                CANMsgObjectConfig(SOC_DCAN_1_REGS, &entry);
            }
        }
    }
}

/* Interrupt mapping to AINTC and registering CAN ISR */
static void DCANAintcConfigure(void)
{
  if (rtems_interrupt_handler_install(55, "can_interrupt", RTEMS_INTERRUPT_UNIQUE, DCANIsr0, NULL) != 0) {
	printf("interrupt registration failed\n");
	return ;
  }

	printf("Interrupt registration successful\n");
    //request_irq(55, DCANIsr0, NULL);
}
