#!/bin/sh

PATH=$PATH:~/linux/development/quick-start/src/rsb/rtems/~/linux/development/quick-start/rtems/6/bin/

./waf

arm-rtems6-objcopy build/arm/beagleboneblack/testsuites/samples/hello.exe -O binary hello.bin
rm hello.bin.gz; gzip -9 hello.bin
mkimage -A arm -O linux -T kernel -a 0x80000000 -e 0x80000000 -n RTEMS -d hello.bin.gz rtems-hello.img

cp rtems-hello.img /tftpboot/
